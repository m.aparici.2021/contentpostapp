import webapp

FORM_TEMPLATE = """
<h1>
    Add a page form:
</h1>
<form action="/addpage" method="post">
    Resource:<br>
    <input type="text" name="resource"><br>
    Resource Content:<br>
    <input type="text" name="content"><br>
    <input type="submit" value="Send">
</form>
"""

ERROR_PAGE = f"""
<!DOCTYPE html>
<html>
    <head>
        <title>Error</title>
        <meta charset="utf-8">
    </head>
    <body>
        <h1>
            NO SUCH PAGE
        </h1>
        {FORM_TEMPLATE}
    </body>
</html>
 """

MAIN_PAGE = f"""
<!DOCTYPE html>
<html>
    <head>
        <title>add page form</title>
        <meta charset="utf-8">
    </head>
    <body>
        <h1>
            This is the main page
        </h1>
        {FORM_TEMPLATE}
    </body>
</html>
"""


class ContentPostApp(webapp.WebApp):

    content_dict = {"/": MAIN_PAGE}

    def parse(self, request):
        try:
            serve = ERROR_PAGE
            code = "404 Not Found"
            # parse the request fields
            parse = request.split("\r\n")
            # get the request's method
            method = parse[0].split(" ")[0]
            resource = parse[0].split(" ")[1]
            if method == "GET":
                all_resources = self.content_dict.keys()
                if resource in all_resources:
                    serve = self.content_dict[resource]
                    code = "200 OK"
            elif method == "POST":
                if resource == "/addpage":
                    new_resource = parse[len(parse) - 1].split("&")[0].split("=")[1]
                    if "/" not in new_resource:
                        new_resource = "/"+new_resource
                    if new_resource not in self.content_dict.keys():
                        new_rsrc_content = parse[len(parse) - 1].split("&")[1].split("=")[1].replace("+", " ")
                        content_str = "<body><p1>"+new_rsrc_content+"</p1>"+FORM_TEMPLATE+"</body>"
                        self.content_dict[new_resource] = content_str
                    serve = self.content_dict[new_resource]
                    code = "200 OK"
            serve += "<h1>Current Resources</h1>""<p1>"+str(self.content_dict.keys())+"</p2>"
        except KeyError:
            serve = ERROR_PAGE
            code = "404 Not Found"
        return [serve, code]

    def process(self, parsedRequest):
        page = parsedRequest[0]
        code = parsedRequest[1]
        return (code, page)


if __name__ == "__main__":
    ContentPostApp("127.0.0.1",1234)
